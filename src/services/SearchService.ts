import { Service } from 'typedi';
import { OrmRepository } from "typeorm-typedi-extensions";

import { ISearchService } from "./abstract/ISearchService"
import { Order } from '../orm/entities/Order';
import { OrderRepository } from '../orm/repositories/OrderRepository';
import { Search } from '../orm/entities/Search';

@Service("SearchService")
export class SearchService {
    
    @OrmRepository() private orderRepository: OrderRepository;

    constructor() {}

    Post(search: Search): Promise<Order> {
       return this.orderRepository.CreationFromSearchRequest(search);
    }

}