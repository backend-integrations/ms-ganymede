import { promises } from "fs";
import { ObjectID } from "typeorm";

import { Order } from "../../orm/entities/Order";
import { Search } from "../../orm/entities/Search";

export interface ISearchService {
    Post(search: Search): Promise<Order>;
}