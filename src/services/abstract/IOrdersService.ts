import { promises } from "fs";
import { ObjectID } from "typeorm";

import { Order } from "../../orm/entities/Order";

export interface IOrdersService {
    GetList(): Promise<Order[]>;
    Get(id: ObjectID): Promise<Order>;
}