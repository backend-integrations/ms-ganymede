import { EntityRepository, FindManyOptions, Repository } from 'typeorm';
import { Order } from '../entities/Order';
import { Search } from '../entities/Search';
import { Status } from '../entities/enums/Status';
import { Service } from 'typedi';

@Service()
@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {

  public async CreationFromSearchRequest(search: Search): Promise<Order> {

    const order = new Order();
    order.search = search;
    order.status = Status.RECEIVED;

    return this.save(order);
    
  }

}