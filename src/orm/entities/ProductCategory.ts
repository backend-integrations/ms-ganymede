import {Column, Entity, ObjectID, ObjectIdColumn, OneToMany} from "typeorm";
import { Product } from "./Product";
import { Transform } from 'class-transformer';
import toHexString from "../../tools/toHexString";

@Entity()
export class ProductCategory {

    @ObjectIdColumn()
    @Transform(toHexString, {toPlainOnly: true})
    id: ObjectID;

    @Column()
    keysValues: string;

    @Column()
    url: string;

    @Column(type => Product)
    Products: Product[];
    
}
