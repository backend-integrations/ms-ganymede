export class ProductModel {

    typeCodeIdentification: string;

    CodeIdentification: string;

    description: string;

    name: string;

    price: string;

    discount: string;

    usedSearchTerm: string;

    imgUrl: string[];

    uri: string;

}