import "reflect-metadata";
import * as dotenv from "dotenv";
import { Container } from "typedi";
import { createKoaServer, useContainer as routingUseContainer } from "routing-controllers";
import { useContainer as typeormUseContainer, createConnection, ConnectionOptions } from "typeorm";
//import * as Swagger from "koa2-swagger-ui";
const koaSwagger = require('koa2-swagger-ui'); // TODO Import * Error
import Queue = require("bull");
import Arena = require("bull-arena");

if (process.env.NODE_ENV !== "production") {
    // Used only in development to load environment variables from local file.
    dotenv.config();
}

console.log(`Server start on ${process.env.NODE_ENV}`);

typeormUseContainer(Container);
routingUseContainer(Container);

//import connection from "./orm/Database";
import connectionOpts from "./orm/Database";

// creates Koa app
const app = createKoaServer({
    routePrefix: "/api/product",
    cors: true,
    controllers: [`${__dirname}/controllers/*{.ts,.js}`],
    middlewares: [`${__dirname}/middlewares/*{.ts,.js}`]
});

// TODO Bug not found
app.use(koaSwagger({
    routePrefix: '/swagger',
    swaggerOptions: {
      url: '/swagger.yml'
    }
  })
);

/* 
console.log(`TYPEORM start on Host ${process.env.TYPEORM_HOST} | Port ${process.env.TYPEORM_PORT}`);

const connectionOpts: ConnectionOptions = {
    type: 'mongodb',
    host: process.env.TYPEORM_HOST || 'localhost',
    port: Number(process.env.TYPEORM_PORT) || 27017,
    database: process.env.TYPEORM_DATABASE || 'ganymede',
    username: process.env.TYPEORM_USERNAME || '',
    password: process.env.TYPEORM_PASSWORD || '',  
    entities: [
      `${__dirname}/orm/entities/*{.ts,.js}`,
    ]
  };

const connection:Promise<Connection> = createConnection(connectionOpts);
 */

createConnection(connectionOpts)
  .then(async connection => {
    app.listen(process.env.PORT || 1300);
    console.log(`Server Running on ${process.env.PORT}`);
  })
  .catch(
    error => {
      console.log("Error: ", error);
    }      
  );

