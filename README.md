# MS-Ganymede

MicroService Ganymede - Soporte

We also promote the `Single Responsibility`

![Back End Architecture](doc/img/Back-End-Architecture.png)

The `Ecosystem` will be formed.

![Context](doc/img/Context.png)

**Libraries and framework**
* [koa](https://github.com/koajs/koa)
* [TypeORM](https://github.com/typeorm/typeorm)
* [TypeDI](https://github.com/typestack/typedi) 
* [Routing Controllers](https://github.com/typestack/routing-controllers)
* [TypeDI Service container integration with TypeORM](https://github.com/typeorm/typeorm-typedi-extensions)
* [Bull - Redis-based queue for Node](https://github.com/OptimalBits/bull)
* [dotenv](https://github.com/motdotla/dotenv)
* [swagger koa](https://github.com/janvotava/swagger-koa) 

**Documentation**
* [Debugger](doc/debugger.md)
* [Queue](doc/queue.md)
 
# Installer

*Context*
> npm install -g typescript

*Debug and Development*
> npm install -g nodemon

> npm install -g ts-node

# Hosting
* API REST *Node* [Heroku](https://dashboard.heroku.com/login).
* DB *Mongoose* [Mlab](https://mlab.com/).
* AMQP *Redis* [Redislabs](https://app.redislabs.com)

# Configurations

**Environment Variables**

If you are working in a local environment, use the file `.env`.
In case of publishing the application in a productive environment, configure it through the `environment variables`.

**experimentalDecorators Error**

* 1 Go to File => Preferences => Settings (or Control+comma) and it will open the User Settings file. Add "javascript.implicitProjectConfig.experimentalDecorators": true

* 2 Move File tsconfig.json in src

* 3 Close VsCode.

#

# heroku

Name Heroku: sirena-ms-ganymede

host-dev: https://sirena-ms-ganymede.herokuapp.com

**Configure environment variables**

Example CLI:
> heroku config:set REDIS_HOST=redis-*****.cloud.redislabs.com


Know Your Foe: Would You Like to Know **More**?

* Debugging 
  * [How to get auto restart and breakpoint support with Typescript and Node](https://medium.com/aherforth/how-to-get-auto-restart-and-breakpoint-support-with-typescript-and-node-5af589dd8687)
  * [Debugging JavaScript/TypeScript Node apps with Chrome DevTools, VS Code and WebStorm](https://hackernoon.com/debugging-javascript-typescript-node-apps-with-chrome-devtools-vs-code-and-webstorm-97b882aee0ad)
* Environment Variables 
  * [Configuration settings in Node with dotenv](https://medium.com/@jonjam/configuration-settings-in-node-with-dotenv-6c6e46470046)
  * [Using dotenv package to create environment variables](https://medium.com/@thejasonfile/using-dotenv-package-to-create-environment-variables-33da4ac4ea8f)
  * [How to read environment variables in Node.js?](https://fullstack-developer.academy/how-to-read-environment-variables-in-node-js/)
* Best Practices [Best Practices for Designing a Pragmatic RESTful API](https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api)
* AMQP 
  * [Advanced Message Queuing Protocol](https://es.wikipedia.org/wiki/Advanced_Message_Queuing_Protocol)
  * [Bring Redux to your queue logic: an Express setup with ES6 and bull queue](https://codewithhugo.com/bring-redux-to-your-queue-logic-an-express-setup-with-es6-and-bull-queue/#processing-jobs)
  * [Bull's Guide](https://optimalbits.github.io/bull/)
* Deploy
  * [Deploy your application to Heroku](https://devcenter.heroku.com/articles/deploying-nodejs#specifying-a-start-script)